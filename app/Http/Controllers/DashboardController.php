<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function versionone()
    {
        return view('dashboard.v1');
    }
    public function versiontwo()
    {
        return view('dashboard.v2');
    }
    public function versionthree()
    {
        return view('dashboard.v3');
    }

    public function uploadpdf()
    {
        return view('dashboard.pdf');
    }

    public function upload(Request $request){
        $file = $request->file('pdf_file');
        if($file) {
            $extension = $request->file('pdf_file')->extension();
            if($extension != 'pdf'){
                return back()->with('error','Only pdf files are allowed!');
            }
            $filename = time() . '.' . $extension;
            $filePath = public_path() . '/pdf/';
            $file->move($filePath, $filename);
            $data=array('pdf_file'=>$filename);
            if(DB::table('pdf_uploads')->insert($data)){
                return back()->with('success','PDF uploaded successfully!');
            } else {
                return back()->with('error','There is some error while uloading pdf. Please try again!');
            }
        }  else {
            return back()->with('error','Please select any pdf file!');
        }
    }
    public function viewpdf()
    {

        $pdfs = DB::table('pdf_uploads')->get();
        $pdfArray = [];
        if (!$pdfs->isEmpty()) {
            foreach($pdfs as $pdf){
                $link = asset('pdf/'.$pdf->pdf_file);
                $pdfArray[] = array(
                    'file_name' => $pdf->pdf_file,
                    'action'    => '<a target="_blank" href="'.$link.'">Download PDF</a> | <a target="_blank" href="'.route('edit-pdf',$pdf->id).'">Edit PDF</a>'
                );
            }
        }
        return view('dashboard.viewpdf',['pdfArray' => $pdfArray]);
    }

    public function editPdf($pdf_id){

        $pdfInfo = DB::table('pdf_uploads')->find($pdf_id);
        //print_r($pdfInfo); die;
        return view('dashboard.edit_pdf',compact('pdfInfo'));
    }

    public function postEditPdf(Request $request){

        $file   =   $request->file();
        $avatar =   $file['updated_pdf'];
        $extension = $avatar->extension();
        
        if($extension != 'pdf'){
            return back()->with('error','Only pdf files are allowed!');
        }
        
        $filename = time() . '.' . $extension;
        $filePath = public_path() . '/pdf/';
        $avatar->move($filePath, $filename);
        $data   =   array('pdf_file'=>$filename);

        $pdfInfo = DB::table('pdf_uploads')->where('id',$request->pdf_id)->first();
        if($pdfInfo){

            @unlink($filePath.$pdfInfo->pdf_file);
            DB::table('pdf_uploads')->where('id',$request->pdf_id)->update($data);
            //$pdfInfo->pdf_file = $filename;
            //$pdfInfo->save();
        }

        return response()->json(['status' => 1,'message' => 'File has been saved successfully.']);

    }
}
