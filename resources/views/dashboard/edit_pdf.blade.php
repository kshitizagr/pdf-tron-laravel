@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">View PDF</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12 col-12">
            <script src='{{ asset('WebViewer/lib/webviewer.min.js') }}'> </script>

            <div id='viewer' style='width: 1024px; height: 600px;'> </div>
            <div class="form-control">
                <button type="button" class="btn-primary" onclick="uploadEditedFile()">Save</button>
            </div>

            <script>
                
                const viewerElement = document.getElementById('viewer');

                const blob_data = instanceI = null;

                WebViewer({
                    path: "{{ asset('WebViewer/lib') }}",
                    initialDoc: "{{ asset('pdf/'.$pdfInfo->pdf_file) }}", // replace with your own PDF file
                    fullAPI: true,
                }, viewerElement).then((instance) => {
                    instanceI=instance;
                    // call apis here
                    //console.log(1212);
                    const { docViewer, annotManager } = instance;

                   
                    // docViewer.on('documentLoaded', async () => {
                    //     console.log(12121212);
                    //     const doc = docViewer.getDocument();
                    //     const xfdfString = await annotManager.exportAnnotations();
                    //     const data = await doc.getFileData({
                    //         // saves the document with annotations in it
                    //         xfdfString
                    //     });

                    //     const arr = new Uint8Array(data);
                    //     const blob = new Blob([arr], { type: 'application/pdf' });
                    //     //console.log(11,blob);
                    //     // add code for handling Blob here
                    //    // uploadEditedFile(blob);
                    //     //blob_data=blob;

                    // });

                    annotManager.on('annotationChanged', (annotations, action) => {
                        // console.log("annotation changed");
                        // if (action === 'add') {
                        //     console.log('this is a change that added annotations');
                        // } else if (action === 'modify') {
                        //     console.log('this change modified annotations');
                        // } else if (action === 'delete') {
                        //     console.log('there were annotations deleted');
                        // }

                        // const doc = docViewer.getDocument();
                        // const xfdfString = annotManager.exportAnnotations();
                        // const data = doc.getFileData({
                        //     // saves the document with annotations in it
                        //     xfdfString
                        // });

                        // const arr = new Uint8Array(data);
                        // const blob = new Blob([arr], { type: 'application/pdf' });
                        // var formData = new FormData();
                        // formData.append('avatar', blob);
                        // formData.append('_token', '{{ csrf_token() }}');
                        // //uploadEditedFile(blob);
                        // $.post({
                        //     url: "{{ route('post-edit-pdf') }}",
                        //     data: formData,
                        // }).done(function(data) {
                        //     console.log(data);
                        // });

                        /*annotations.forEach((annot) => {
                            console.log('annotation page number', annot.PageNumber);
                        });*/
                    });

                    /*docViewer.on('layoutChanged', async () => {
                        console.log(444444);
                    });*/

                });

                async function uploadEditedFile() {
                    
                    //console.log("Entered",blob_data,instanceI);
                    const { docViewer, annotManager } = instanceI;

                    const doc = docViewer.getDocument();
                    const xfdfString = await annotManager.exportAnnotations();
                    const data = await doc.getFileData({
                        // saves the document with annotations in it
                        xfdfString
                    });

                    const arr = new Uint8Array(data);
                    const blob = new Blob([arr], { type: 'application/pdf' });
                    //console.log(11,blob);

                    // add code for handling Blob here
                    
                    var formData = new FormData();

                    formData.append('updated_pdf', blob);
                    formData.append('_token', '{{ csrf_token() }}');
                    formData.append('pdf_id', '{{ $pdfInfo->id }}');
                    
                    // sendRequestToAjax(formData);

                    const req = new XMLHttpRequest();
                    req.open("POST", "{{ route('post-edit-pdf') }}", true);
                        req.onload = function (oEvent) {
                        // Uploaded.
                    };

                    req.send(formData);
                   
                    /*const doc = docViewer.getDocument();
                    const xfdfString = await annotManager.exportAnnotations();
                    const options = { xfdfString };
                    const data = await doc.getFileData(options);
                    const arr = new Uint8Array(data);
                    const blob = new Blob([arr], { type: 'application/pdf' });
                    var formData = new FormData();
                    formData.append('avatar', blob);*/
                    // var formData = new FormData();
                    // formData.append('avatar', 233);

                    // $.post({
                    //     url: "{{ route('post-edit-pdf') }}",
                    //     data: formData,
                    // }).done(function(data) {
                    //     console.log(data);
                    // });
                }
                /*$(document).ready(function () {
                    alert(2323);
                    $.post({
                        url: "{{ route('post-edit-pdf') }}",
                        data: {avatar:"13333"},
                    }).done(function(data) {
                        console.log(data);
                    });

                });*/


             /*function sendRequestToAjax(data){

                //alert(1223); return false;
                $.ajax({
                type: "POST",
                url: "{{ route('post-edit-pdf') }}",
                data: data,
                cache: false,
                success: function(data){
              
                  console.log("dfdsfsd",{data});
               }
            });
                 
             }*/


            </script>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
