<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return bcrypt('123456');
});

Auth::routes();

Route::get('/dashboard/home', 'DashboardController@versionone')->name('home');
Route::get('/dashboard/v2', 'DashboardController@versiontwo')->name('v2');
Route::get('/dashboard/v3', 'DashboardController@versionthree')->name('v3');

Route::get('/dashboard/uploadpdf', 'DashboardController@uploadpdf')->name('uploadpdf');
Route::post('/dashboard/upload', 'DashboardController@upload')->name('uploadfile');
Route::get('/dashboard/viewpdf', 'DashboardController@viewpdf')->name('pdf');
Route::get('/dashboard/edit-pdf/{pdf_id}', 'DashboardController@editPdf')->name('edit-pdf');
Route::post('/dashboard/edit-pdf', 'DashboardController@postEditPdf')->name('post-edit-pdf');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
